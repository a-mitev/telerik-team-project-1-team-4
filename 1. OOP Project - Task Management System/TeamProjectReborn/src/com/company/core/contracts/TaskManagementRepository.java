package com.company.core.contracts;

import com.company.models.tasks.contracts.*;
import com.company.models.tasks.enums.*;
import com.company.models.teams.contracts.Board;
import com.company.models.teams.contracts.Member;
import com.company.models.teams.contracts.Team;

import java.util.List;

public interface TaskManagementRepository {

    String createNewMember(String name);

    String createNewTeam(String teamName);

    String createNewStory (String teamName, String boardName,
                           String title, String description,
                           Priority priority, StorySize size, String assignee);

    String createNewBug(String teamName, String boardName,
                        String title, String description, List<String> stepsToReproduce,
                        Priority priority, BugSeverity severity, String assignee);


    String createNewFeedback(String teamName, String boardName,
                             String title, String description, int rating);



    Team findTeamByName(String teamName);

    Member findMemberByName(String memberName);

    Bug findBugByTitle(String bugTitle);

    Story findStoryByTitle(String storyTitle);

    Feedback findFeedbackByTitle(String feedbackTitle);



    List <Member> getMembers();

    List<Team> getTeams();

    List<Task> getTasks();

    List<AssignableTask> getAssignableTasks();



    List<Bug> getBugs();

    List<Story> getStories();

    List<Feedback> getFeedbacks();

    AssignableTask findAssignableTask(int assignableTaskId);

    Task findTaskById(int taskId);

    void checkIfAssigneeIsValid(String teamName,String assignee);

    void checkIfMemberIsFromTeam(String memberName,String teamName);

    void addTaskToBoard(Task task, String boardName, String teamName);

    void addTaskToMember(AssignableTask task, String assignee);

    Board findBoardInTeam(String boardName, String teamName);

    void validateUserIsFromTeam(String userName, String teamName);

    void validateUserAndTaskFromSameTeam(String userName, int taskId);
}
