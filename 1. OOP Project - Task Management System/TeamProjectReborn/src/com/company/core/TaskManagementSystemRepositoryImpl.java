package com.company.core;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.BugImpl;
import com.company.models.tasks.FeedbackImpl;
import com.company.models.tasks.StoryImpl;
import com.company.models.tasks.contracts.*;
import com.company.models.tasks.enums.*;
import com.company.models.teams.MemberImpl;
import com.company.models.teams.TeamImpl;
import com.company.models.teams.contracts.Board;
import com.company.models.teams.contracts.Member;
import com.company.models.teams.contracts.Team;

import static com.company.models.tasks.AssignableTaskImpl.UNASSIGNED;

import java.util.ArrayList;
import java.util.List;

public class TaskManagementSystemRepositoryImpl implements TaskManagementRepository {

    private static final String ALREADY_EXISTS = "This %s name already exists! Please choose a unique %s name.";
    public static final String TEAM_ADDED_SUCCESSFULLY = "Team %s created successfully.";
    public static final String MEMBER_CREATED_SUCCESSFULLY = "User %s created successfully.";
    public static final String MEMBER_ALREADY_EXISTS = String.format(ALREADY_EXISTS, "user", "user");

    private final static String NO_SUCH_MEMBER = "There is no member with name %s!";
    private final static String NO_SUCH_TEAM = "There is no team with name %s!";
    private final static String NO_SUCH_BOARD = "There is no board with name %s!";
    private final static String NO_SUCH_BUG = "There is no bug with name %s!";
    private final static String NO_SUCH_STORY = "There is no bug with name %s!";
    private final static String NO_SUCH_FEEDBACK = "There is no feedback with name %s!";
    public static final String MEMBER_NOT_IN_TEAM = "Member %s does not exist in team %s.";
    public static final String INVALID_ID = "Invalid task ID.";
    public static final String USER_NOT_FROM_TEAM = "The user should be a member of the team!";
    public static final String USER_OR_TASK_NOT_FROM_TEAM = "User and task should be from the same team!";

    public static final String CREATOR_SHOULD_BE_FROM_TEAM = "The task creator should be a member of the team!";
    public static final String TASK_ADDED_TO_BOARD =
            "%s with ID %d was successfully created and added to board %s in team %s.";

    private static final String NOT_EXIST = "The %s does not exist! Create a %s with this name first.";
    public static final String BOARD_DOES_NOT_EXIST = String.format(NOT_EXIST, "board", "board");
    public static final String MEMBER_DOES_NOT_EXIST = String.format(NOT_EXIST, "member", "member");

    private final List<Team> teams;
    private final List<Member> members;
    private final List<Bug> bugs;
    private final List<Story> stories;
    private final List<Feedback> feedbacks;

    private static int nextTaskID = 1;

    public TaskManagementSystemRepositoryImpl() {
        this.teams = new ArrayList<>();
        this.members = new ArrayList<>();
        this.bugs = new ArrayList<>();
        this.stories = new ArrayList<>();
        this.feedbacks = new ArrayList<>();
    }

    @Override
    public List<Team> getTeams() { return new ArrayList<>(teams); }
    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }
    @Override
    public List<Bug> getBugs() {
        return new ArrayList<>(bugs);
    }
    @Override
    public List<Story> getStories() {
        return new ArrayList<>(stories);
    }
    @Override
    public List<Feedback> getFeedbacks() {
        return new ArrayList<>(feedbacks);
    }

    @Override
    public List<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(bugs);
        tasks.addAll(stories);
        tasks.addAll(feedbacks);
        return new ArrayList<>(tasks);
    }
    @Override
    public List<AssignableTask> getAssignableTasks() {
        List<AssignableTask> assignableTasks = new ArrayList<>();
        assignableTasks.addAll(bugs);
        assignableTasks.addAll(stories);
        return new ArrayList<>(assignableTasks);
    }


    @Override
    public String createNewMember(String name) {
        if (getMembers().stream().anyMatch(member -> member.getName().equals(name))) {
            throw new InvalidUserInputException(MEMBER_ALREADY_EXISTS);
        }
        Member member = new MemberImpl(name);
        members.add(member);
        return String.format(MEMBER_CREATED_SUCCESSFULLY, member.getName());
    }

    @Override
    public String createNewTeam(String teamName) {
        Team team = new TeamImpl(teamName);
        teams.add(team);
        return String.format(TEAM_ADDED_SUCCESSFULLY, teamName);
    }

    @Override
    public String createNewBug(String teamName, String boardName,
                               String title, String description, List<String> stepsToReproduce,
                               Priority priority, BugSeverity severity, String assignee) {
        Bug bug = new BugImpl(nextTaskID, title, description, stepsToReproduce, priority, severity);
        addTaskToBoard(bug, boardName, teamName);
        if (!assignee.isEmpty()) {
            addTaskToMember(bug, assignee);
        }
        bugs.add(bug);
        nextTaskID++;
        return String.format(TASK_ADDED_TO_BOARD, "Bug", bug.getId(), boardName, teamName);
    }

    @Override
    public String createNewStory(String teamName, String boardName,
                                 String title, String description,
                                 Priority priority, StorySize size, String assignee) {
        Story story = new StoryImpl(nextTaskID, title, description, priority, size);
        addTaskToBoard(story, boardName, teamName);
        if (!assignee.isEmpty()) {
            addTaskToMember(story, assignee);
        }
        stories.add(story);
        nextTaskID++;
        return String.format(TASK_ADDED_TO_BOARD, "Story", story.getId(), boardName, teamName);
    }

    @Override
    public String createNewFeedback(String teamName, String boardName,
                                    String title, String description,
                                    int rating) {
        Feedback feedback = new FeedbackImpl(nextTaskID, title, description, rating);
        addTaskToBoard(feedback, boardName, teamName);
        feedbacks.add(feedback);
        nextTaskID++;
        return String.format(TASK_ADDED_TO_BOARD, "Feedback", feedback.getId(), boardName, teamName);

    }

    @Override
    public Member findMemberByName(String memberName) {
        Member member = members
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(memberName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_MEMBER, memberName)));
        return member;
    }

    @Override
    public Team findTeamByName(String teamName) {
        Team team = teams
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(teamName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_TEAM, teamName)));
        return team;
    }

    @Override
    public Bug findBugByTitle(String bugTitle) {
        Bug bug = bugs
                .stream()
                .filter(u -> u.getTitle().equalsIgnoreCase(bugTitle))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_BUG, bugTitle)));
        return bug;
    }

    @Override
    public Story findStoryByTitle(String storyTitle) {
        Story story = stories
                .stream()
                .filter(u -> u.getStoryTitle().equalsIgnoreCase(storyTitle))
                .findFirst()
                //change
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_STORY, storyTitle)));
        return story;
    }

    @Override
    public Feedback findFeedbackByTitle(String feedbackTitle) {
        Feedback feedback = feedbacks
                .stream()
                .filter(u -> u.getTitle().equalsIgnoreCase(feedbackTitle))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(NO_SUCH_FEEDBACK, feedbackTitle)));
        return feedback;
    }


    @Override
    public void checkIfAssigneeIsValid(String teamName, String assignee) {
        if (!assignee.equals(UNASSIGNED)) {
            findTeamByName(teamName).getMembers()
                    .stream()
                    .filter(member -> member.getName().equals(assignee))
                    .findFirst()
                    .orElseThrow(() -> new InvalidUserInputException(
                            String.format(MEMBER_NOT_IN_TEAM, assignee, teamName)));
        }
    }

    @Override
    public void checkIfMemberIsFromTeam(String memberName, String teamName) {
        Member user = findMemberByName(memberName);
        Team team = findTeamByName(teamName);
        if (!team.getMembers().contains(user)) {
            throw new InvalidUserInputException(CREATOR_SHOULD_BE_FROM_TEAM);
        }
    }

    public void addTaskToBoard(Task task, String boardName, String teamName) {
        Board board = findBoardInTeam(boardName, teamName);
        board.addTask(task);
    }

    @Override
    public void addTaskToMember(AssignableTask task, String assignee) {
        Member member = members.stream()
                .filter(u -> u.getName().equals(assignee))
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(MEMBER_DOES_NOT_EXIST));
        //HERE the MAGIC happens.
        member.assignTask(task);
    }

    @Override
    public Board findBoardInTeam(String boardName, String teamName) {
        Team team = findTeamByName(teamName);
        return team.getBoards()
                .stream()
                .filter(board -> board.getName().equals(boardName))
                .findFirst()
                .orElseThrow(() -> new InvalidUserInputException(BOARD_DOES_NOT_EXIST));
    }

    @Override
    public void validateUserIsFromTeam(String userName, String teamName) {
        Member user = findMemberByName(userName);
        Team team = findTeamByName(teamName);
        if (!team.getMembers().contains(user)) {
            throw new InvalidUserInputException(USER_NOT_FROM_TEAM);
        }
    }

    @Override
    public void validateUserAndTaskFromSameTeam(String userName, int taskId) {
        Member user = findMemberByName(userName);
        Task task = findAssignableTask(taskId);
        for (Team team : teams) {
            if (team.getMembers().contains(user) && team.containsTask(task)) {
                return;
            }
        }
        throw new InvalidUserInputException(USER_OR_TASK_NOT_FROM_TEAM);
    }

    @Override
    public Task findTaskById(int taskId) {
        return genericTaskFinder(taskId, getTasks());
    }

    @Override
    public AssignableTask findAssignableTask(int assignableTaskId) {
        return genericTaskFinder(assignableTaskId, getAssignableTasks());
    }

    private <T extends Task> T genericTaskFinder(int taskID, List<T> tasks) {
        return tasks.stream()
                .filter(task -> task.getId() == taskID)
                .findAny()
                .orElseThrow(() -> new InvalidUserInputException(INVALID_ID));
    }
}
