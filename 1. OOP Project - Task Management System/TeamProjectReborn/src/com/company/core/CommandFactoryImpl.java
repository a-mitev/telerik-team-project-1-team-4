package com.company.core;

import com.company.commands.*;
import com.company.commands.contracts.Command;
import com.company.commands.creation.*;
import com.company.commands.enums.CommandType;
import com.company.commands.show.*;
import com.company.core.contracts.CommandFactory;
import com.company.core.contracts.TaskManagementRepository;
import com.company.utils.ParsingHelpers;

public class CommandFactoryImpl implements CommandFactory {

    @Override
    public Command createCommandFromCommandName(String commandTypeAsString, TaskManagementRepository taskManagementRepository) {
        CommandType commandType = ParsingHelpers.tryParseEnum(commandTypeAsString, CommandType.class);

        switch (commandType) {
            case CREATETEAM:
                return new CreateTeamCommand(taskManagementRepository);
            case CREATEMEMBER:
                return new CreateNewMember(taskManagementRepository);
            case ADDMEMBERTOTEAM:
                return new AddMemberToTeamCommand(taskManagementRepository);
            case CREATEBOARD:
                return new CreateBoardCommand(taskManagementRepository);
            case CREATEBUG:
                return new CreateBugCommand(taskManagementRepository);
            case CHANGEBUG:
                return new ChangeBugCommand(taskManagementRepository);
            case CREATESTORY:
                return new CreateStoryCommand(taskManagementRepository);
            case CHANGESTORY:
                return new ChangeStoryCommand(taskManagementRepository);
            case CREATEFEEDBACK:
                return new CreateFeedbackCommand(taskManagementRepository);
            case CHANGEFEEDBACK:
                return new ChangeFeedbackCommand(taskManagementRepository);
            case SHOWALLUSERS:
                return new ShowAllMembersCommand(taskManagementRepository);
            case SHOWALLTEAMS:
                return new ShowAllTeamsCommand(taskManagementRepository);
            case SHOWALLTASKS:
                return new ShowAllTasksCommand(taskManagementRepository);
            case SHOWTEAMMEMBERS:
                return new ShowAllTeamMembers(taskManagementRepository);
            case SHOWTEAMBOARDS:
                return new ShowTeamBoardsCommand(taskManagementRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivityCommand(taskManagementRepository);
            case SHOWBOARDSACTIVITY:
                return new ShowBoardActivityCommand(taskManagementRepository);
            case SHOWMEMBERACTIVITY:
                return new ShowMemberActivityCommand(taskManagementRepository);
            case SHOWTASKCOMMENTS:
                return new ShowTaskCommentsCommand(taskManagementRepository);
            case ASSIGNTASK:
                return new AssignTaskCommand(taskManagementRepository);
            case ADDCOMMENTTOTASK:
                return new AddCommentToTaskCommand(taskManagementRepository);
            default:
                throw new IllegalArgumentException();
        }
    }

}

