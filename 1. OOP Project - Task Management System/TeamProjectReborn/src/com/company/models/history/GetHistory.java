package com.company.models.history;

import java.util.List;

public interface GetHistory {

    String HISTORY_OF_CHANGES_HEADER = "%s %s activity:";
    String HISTORY_OF_CHANGES_EMPTY = "Nothing to display";

    List<Event> getHistory();
}
