package com.company.models.history;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ActivityLoggerImpl implements ActivityLogger {

    private final List<Event> events;

    public ActivityLoggerImpl() { events = new ArrayList<>(); }

    @Override
    public List<Event> getCompleteHistory() { return new ArrayList<>(events); }

    @Override
    public void addChange(String description) {
        events.add(new Event(description));
    }

    @Override
    public int size() {
        return events.size();
    }


}
