package com.company.models.history;

import java.util.List;

public interface ActivityLogger {

    String CHANGE_MESSAGE = "%s changed from %s to %s.";
    String CREATION_MESSAGE = "Task: %s created.";
    String BOARD_CREATION_MESSAGE = "Board: %s created.";
    String IMPOSSIBLE_CHANGE_MESSAGE = "%s is already %s.";

    List<Event> getCompleteHistory();

    void addChange(String description);

    int size();
}
