package com.company.models.comments;

public interface Comment {

    String getContent();

    String getAuthor();
}
