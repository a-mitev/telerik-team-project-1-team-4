package com.company.models.tasks;

import com.company.models.comments.Comment;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.history.ActivityLogger;
import com.company.models.history.ActivityLoggerImpl;
import com.company.models.history.Event;
import com.company.models.tasks.contracts.Task;
import com.company.models.tasks.contracts.TaskStatus;
import com.company.models.tasks.enums.Tasks;
import com.company.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.company.models.history.ActivityLogger.CREATION_MESSAGE;
import static java.lang.String.format;


public abstract class TaskBaseImpl implements Task {

    public static final int TITLE_MIN_LENGTH = 10;
    public static final int TITLE_MAX_LENGTH = 50;
    public static final String TITLE_ERROR_MESSAGE = format(
            "A title cannot be less than %d and more than %d characters.",
            TITLE_MIN_LENGTH,TITLE_MAX_LENGTH);

    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 500;
    public static final String DESCRIPTION_ERROR_MESSAGE = format(
            "A description cannot have less than %d and more than %d characters.",
            DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH);

    public static final String COMMENTS_HEADER = "- *COMMENTS* -";
    public static final String HISTORY_HEADER = "- *HISTORY* -";
    public static final String HISTORY_FOOTER = "-****************-";
    public static final String NO_COMMENTS_HEADER = "- *NO COMMENTS* -";
    public static final String DUPLICATE = "%s is already %s.";
    public static final String CHANGE = "%s changed from %s to %s.";


    private final int id;
    private final List<Comment> comments;
    private final ActivityLogger historyOfChanges;
    private String title;
    private String description;
    private TaskStatus status;


    public TaskBaseImpl(int id, Tasks taskType, String title, String description, TaskStatus status) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        setStatus(status);
        this.comments = new ArrayList<>();
        this.historyOfChanges = new ActivityLoggerImpl();
        addChangeToHistory(String.format(CREATION_MESSAGE,taskType.toString()));
    }

    @Override
    public String getTitle() {return title;}

    @Override
    public String getDescription() {return this.description;}

    @Override
    public String getStatus() {
        return status.toString();
    }

    @Override
    public void setStatus(TaskStatus status) {
        if (this.status == null) {
            this.status = status;
            return;
        }
        if (this.status.equals(status)) {
            throw new InvalidUserInputException(String.format(DUPLICATE, "Status", getStatus()));
        }
        historyOfChanges.addChange(String.format(CHANGE, "Status", this.status, status));
        this.status = status;
    }

    @Override
    public int getId() {return id;}

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public List<Event> getHistory() { return historyOfChanges.getCompleteHistory();}

    @Override
    public String displayComments() {
        StringBuilder output = new StringBuilder();

        if (comments.isEmpty()) {
            output.append(NO_COMMENTS_HEADER);
        } else {
            output.append(COMMENTS_HEADER);
            output.append("\n");
            comments.forEach(output::append);
            output.append(COMMENTS_HEADER);
        }
        return output.toString();
    }

    protected void addChangeToHistory(String description) {
        historyOfChanges.addChange(description);
    }

    protected void setTitle(String title) {
        ValidationHelpers.validateStringLength(
                title, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TITLE_ERROR_MESSAGE);
        this.title=title;

    }

    protected void setDescription(String description) {
        ValidationHelpers.validateStringLength(
                description, DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH,DESCRIPTION_ERROR_MESSAGE);
        this.description = description;
    }
}
