package com.company.models.tasks.enums;

public enum StorySize {
    LARGE,
    MEDIUM,
    SMALL;
    @Override
    public String toString() {
        switch (this) {
            case SMALL:
                return "Small";
            case MEDIUM:
                return "Medium";
            case LARGE:
                return "Large";
            default:
                throw new IllegalArgumentException("Unreachable exception - Size Enum");
        }
    }
}
