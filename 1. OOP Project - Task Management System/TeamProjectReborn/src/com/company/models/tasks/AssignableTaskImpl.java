package com.company.models.tasks;

import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.AssignableTask;
import com.company.models.tasks.contracts.TaskStatus;
import com.company.models.tasks.enums.Priority;
import com.company.models.tasks.enums.Tasks;

import static com.company.models.history.ActivityLogger.CHANGE_MESSAGE;

public abstract class AssignableTaskImpl extends TaskBaseImpl implements AssignableTask {

    public static final String LOWER_BOUNDARY = "Cannot decrease %s further than %s.";
    public static final String UPPER_BOUNDARY = "Cannot increase %s beyond %s.";
    public static final String UNASSIGNED = "Unassigned";

    private Priority priority;
    private String assignee;

    public AssignableTaskImpl(int id, Tasks taskType, String title, String description,
                              Priority priority, TaskStatus status) {
        super(id, taskType, title, description,status);
        setPriority(priority);
        setAssignee(UNASSIGNED);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }

    @Override
    public void setPriority(Priority priority) {
        if (this.priority == null) {
            this.priority = priority;
            return;
        }
        if (this.priority.equals(priority)) {
            throw new InvalidUserInputException(String.format(DUPLICATE, "Priority", this.priority));
        }
        addChangeToHistory(String.format(CHANGE, "Priority", this.priority, priority));
        this.priority = priority;

    }

    @Override
    public void setAssignee(String assignee) {
        if (this.assignee == null) {
            this.assignee = assignee;
        }
        this.assignee = assignee;
    }
    @Override
    public void unAssign() {
        setAssignee(UNASSIGNED);
    }
    //Methods where never used because in our minds priority was going to be changed trough the setter anyway.
//    // Never Used
//    @Override
//    public void increasePriority() {
//        switch (priority) {
//            case LOW:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Priority", priority, Priority.MEDIUM));
//                priority = Priority.MEDIUM;
//                break;
//            case MEDIUM:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Priority", priority, Priority.HIGH));
//                priority = Priority.HIGH;
//                break;
//            case HIGH:
//                throw new InvalidUserInputException(String.format(UPPER_BOUNDARY, "priority", Priority.HIGH));
//        }
//    }
//    //Never Used
//    @Override
//    public void decreasePriority() {
//        switch (priority) {
//            case LOW:
//                throw new InvalidUserInputException(String.format(LOWER_BOUNDARY, "priority", Priority.LOW));
//            case MEDIUM:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Priority", priority, Priority.LOW));
//                priority = Priority.LOW;
//                break;
//            case HIGH:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Priority", priority, Priority.MEDIUM));
//                priority = Priority.MEDIUM;
//                break;
//        }

//    }


}
