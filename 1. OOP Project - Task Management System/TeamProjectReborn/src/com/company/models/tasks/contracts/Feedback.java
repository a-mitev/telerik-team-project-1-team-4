package com.company.models.tasks.contracts;

import com.company.models.tasks.enums.FeedbackStatus;

public interface Feedback extends Task{

    int getRating();

    void setRating(int rating);

}
