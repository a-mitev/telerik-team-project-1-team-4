package com.company.models.tasks.contracts;

import com.company.models.history.GetHistory;
import com.company.models.tasks.contracts.accessory_contracts.Commentable;
import com.company.models.tasks.contracts.accessory_contracts.Identifiable;
import com.company.models.tasks.contracts.accessory_contracts.Printable;

public interface Task extends Identifiable, Printable, Commentable, GetHistory {

    String getTitle();
    String getDescription();
    String getStatus();
    void setStatus(TaskStatus status);

}
