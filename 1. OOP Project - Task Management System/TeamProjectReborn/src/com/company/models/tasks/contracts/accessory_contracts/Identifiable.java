package com.company.models.tasks.contracts.accessory_contracts;

public interface Identifiable {

    public int getId();
}
