package com.company.models.tasks.contracts.accessory_contracts;

public interface Printable {
    String toString();
}
