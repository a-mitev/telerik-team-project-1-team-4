package com.company.models.tasks.contracts;

import com.company.models.tasks.contracts.accessory_contracts.Prioritisable;

public interface AssignableTask extends Task, Prioritisable {

    String getAssignee();

    void setAssignee(String assignee);

    void unAssign();
}
