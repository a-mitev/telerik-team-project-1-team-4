package com.company.models.tasks.contracts;

public interface TaskStatus {
    String toString();
}
