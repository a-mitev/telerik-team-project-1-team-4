package com.company.models.tasks.contracts.accessory_contracts;

import com.company.models.tasks.enums.Priority;

public interface Prioritisable {

    Priority getPriority();

    void setPriority(Priority priority);

}
