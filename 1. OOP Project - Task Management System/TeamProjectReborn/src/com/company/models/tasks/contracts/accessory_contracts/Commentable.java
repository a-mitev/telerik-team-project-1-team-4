package com.company.models.tasks.contracts.accessory_contracts;

import com.company.models.comments.Comment;

import java.util.List;

public interface Commentable {

    void addComment(Comment comment);

    List<Comment> getComments();

    String displayComments();
}
