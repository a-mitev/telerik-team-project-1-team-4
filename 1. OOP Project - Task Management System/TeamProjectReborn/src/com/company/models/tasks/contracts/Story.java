package com.company.models.tasks.contracts;

import com.company.models.tasks.enums.StorySize;
import com.company.models.tasks.enums.StoryStatus;

public interface Story extends AssignableTask{

    StorySize getSize();
    String getStoryTitle();
    void setSize(StorySize size);
}
