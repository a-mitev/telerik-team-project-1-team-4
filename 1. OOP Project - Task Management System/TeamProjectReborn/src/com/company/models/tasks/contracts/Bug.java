package com.company.models.tasks.contracts;

import com.company.models.tasks.enums.BugSeverity;
import com.company.models.tasks.enums.BugStatus;
import com.company.models.tasks.enums.Priority;

import java.util.List;

public interface Bug extends AssignableTask{

    List<String> getStepsToReproduce();

    BugSeverity getSeverity();

    void setSeverity(BugSeverity severity);

    void changeBug(Priority priority, BugSeverity severity, BugStatus status);


}
