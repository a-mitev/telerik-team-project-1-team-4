package com.company.models.tasks;

import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.Bug;
import com.company.models.tasks.enums.BugSeverity;
import com.company.models.tasks.enums.BugStatus;
import com.company.models.tasks.enums.Priority;
import com.company.models.tasks.enums.Tasks;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends AssignableTaskImpl implements Bug {

    private final List<String> stepsToReproduce;
    private BugSeverity severity;

    public BugImpl(int id, String title, String description, List<String> stepsToReproduce,
                   Priority priority, BugSeverity severity) {

        super(id, Tasks.BUG, title, description, priority, BugStatus.ACTIVE);
        this.stepsToReproduce = stepsToReproduce;
        setSeverity(severity);
    }

    @Override
    public BugSeverity getSeverity() {
        return severity;
    }

    @Override
    public List<String> getStepsToReproduce() {return new ArrayList<>(stepsToReproduce); }

    @Override
    public void setSeverity(BugSeverity severity) {
        if (this.severity == null) {
            this.severity = severity;
            return;
        }
        if (this.severity.equals(severity)) {
            throw new InvalidUserInputException(String.format(DUPLICATE, "Severity", severity));
        }
        addChangeToHistory(String.format(CHANGE, "Severity", this.severity, severity));
        this.severity = severity;
    }

    @Override
    public String toString() {
        return String.format("%s ID: %d - Title: %s - Steps: %d - Priority: %s - Severity: %s - " +
                        "Status: %s - Assignee: %s - Comments: %d",
                this.getClass().getSimpleName().replace("Impl", ""),
                getId(), getTitle(), getStepsToReproduce().size(), getPriority(), getSeverity(),
                getStatus(), getAssignee(), getComments().size());
    }

    //Old method from an idea to an approach.
    @Override
    public void changeBug(Priority priority, BugSeverity severity, BugStatus status) {
        setPriority(priority);
        setSeverity(severity);
        setStatus(status);
    }
    //Methods in comment below are not needed and never used.

//    @Override
//    public void increaseSeverity() {
//        switch (severity) {
//            case MINOR:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Severity", severity, BugSeverity.CRITICAL));
//                severity = BugSeverity.MAJOR;
//                break;
//            case MAJOR:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Severity", severity, BugSeverity.CRITICAL));
//                severity = BugSeverity.CRITICAL;
//                break;
//            case CRITICAL:
//                throw new IllegalUnbindException(String.format(UPPER_BOUNDARY, "severity", BugSeverity.CRITICAL));
//        }

//    }
    //    @Override
//    public void decreaseSeverity() {
//        switch (severity) {
//            case MINOR:
//                throw new InvalidUserInputException(String.format(LOWER_BOUNDARY, "severity", BugSeverity.MINOR));
//            case MAJOR:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Severity", severity, BugSeverity.MINOR));
//                severity = BugSeverity.MINOR;
//                break;
//            case CRITICAL:
//                addChangeToHistory(String.format(CHANGE_MESSAGE, "Severity", severity, BugSeverity.MAJOR));
//                severity = BugSeverity.MAJOR;
//                break;
//        }
//    }
}
