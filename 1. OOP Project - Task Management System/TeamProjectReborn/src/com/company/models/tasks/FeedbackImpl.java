package com.company.models.tasks;


import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.Feedback;
import com.company.models.tasks.enums.FeedbackStatus;
import com.company.models.tasks.enums.Tasks;
import com.company.utils.ValidationHelpers;

import static com.company.models.history.ActivityLogger.CHANGE_MESSAGE;

public class FeedbackImpl extends TaskBaseImpl implements Feedback {

    public static final int RATING_UNINITIALIZED = -1;
    public static final int RATING_MIN = 0;
    public static final int RATING_MAX = 10;
    public static final String INVALID_RATING_MESSAGE = String.format("Rating cannot be less than %d or more than %d",
            RATING_MIN, RATING_MAX);

    private int rating = RATING_UNINITIALIZED;

    public FeedbackImpl(int id, String title, String description, int rating) {
        super(id, Tasks.FEEDBACK, title, description, FeedbackStatus.NEW);
        setRating(rating);
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void setRating(int rating) {
        ValidationHelpers.validateIntRange(rating, RATING_MIN, RATING_MAX, INVALID_RATING_MESSAGE);
        if (this.rating == -1) {
            this.rating = rating;
            return;
        }
        if (this.rating == rating) {
            throw new InvalidUserInputException(String.format(DUPLICATE, "Rating", this.rating));
        }
        addChangeToHistory(String.format(CHANGE, "Rating", this.rating, rating));
        this.rating = rating;
    }


    @Override
    public String toString() {
        return String.format("%s ID: %d - Title: %s - Rating: %s - Status: %s - Comments: %d",
                this.getClass().getSimpleName().replace("Impl", ""),
                getId(), getTitle(), getRating(), getStatus(), getComments().size());
    }
    //Here there were methods in comments that were not needed and never used.
}
