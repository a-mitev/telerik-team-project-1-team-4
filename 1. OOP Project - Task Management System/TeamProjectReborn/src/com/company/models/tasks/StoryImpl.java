package com.company.models.tasks;

import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.Story;
import com.company.models.tasks.enums.Priority;
import com.company.models.tasks.enums.StorySize;
import com.company.models.tasks.enums.StoryStatus;
import com.company.models.tasks.enums.Tasks;

import static com.company.models.history.ActivityLogger.CHANGE_MESSAGE;

public class StoryImpl extends AssignableTaskImpl implements Story {

    private StorySize size;

    public StoryImpl(int id, String title, String description, Priority priority, StorySize size) {
        super(id, Tasks.STORY, title, description, priority, StoryStatus.NOT_DONE);
        setSize(size);
    }

    @Override
    public StorySize getSize() { return size; }

    @Override
    public String getStoryTitle() { return getTitle(); }

    @Override
    public void setSize(StorySize size) {
        if (this.size == null) {
            this.size = size;
            return;
        }
        if (this.size.equals(size)) {
            throw new InvalidUserInputException(String.format(DUPLICATE, "Size", this.size));
        }
        addChangeToHistory(String.format(CHANGE, "Size", this.size, size));
        this.size = size;
    }
    @Override
    public String toString() {
        return String.format("%s ID: %d - Title: %s - Priority: %s - Size: %s - " +
                        "Status: %s - Assignee: %s - Comments: %d",
                this.getClass().getSimpleName().replace("Impl", ""),
                getId(), getTitle(), getPriority(), getSize(),
                getStatus(), getAssignee(), getComments().size());
    }

}
