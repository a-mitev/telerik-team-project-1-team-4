package com.company.models.teams;

import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.history.ActivityLogger;
import com.company.models.history.ActivityLoggerImpl;
import com.company.models.history.Event;
import com.company.models.tasks.contracts.Task;
import com.company.models.teams.contracts.Board;
import com.company.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

import static com.company.models.history.ActivityLogger.BOARD_CREATION_MESSAGE;

public class BoardImpl implements Board {

    public static final int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    private static final String BOARD_NAME_LENGTH_ERROR = String.format(
            "A board cannot have less than %d symbols or more than %d symbols.",
            NAME_MIN_LENGTH, NAME_MAX_LENGTH);

    public static final String BOARD_TASK_ADDED = "%s with ID %d added in board %s.";
    public static final String BOARD_TASK_REMOVED = "%s with ID %d removed from board %s.";

    public static final String TASK_ALREADY_EXIST_IN_BOARD = "%s with %d already exists in board %s";
    public static final String TASK_DOES_NOT_EXIST = "%s with %d does not exist in board %s";

    private final ActivityLogger historyOfChanges;
    private final List<Task> tasks;
    private String name;

    public BoardImpl(String name) {
        this.historyOfChanges = new ActivityLoggerImpl();
        tasks = new ArrayList<>();
        setName(name);
        historyOfChanges.addChange(
                String.format(BOARD_CREATION_MESSAGE,getName()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<Event> getHistory() {
        return historyOfChanges.getCompleteHistory();
    }

    @Override
    public void addTask(Task task) {
        if (tasks.contains(task)) {
            throw new InvalidUserInputException(
                    String.format(TASK_ALREADY_EXIST_IN_BOARD,
                            task.getClass().getSimpleName().replace("Impl", ""),
                            task.getId(),
                            getName()));
        }

        historyOfChanges.addChange(
                String.format(BOARD_TASK_ADDED,
                        task.getClass().getSimpleName().replace("Impl", ""),
                        task.getId(),
                        getName()));

        tasks.add(task);
    }

    @Override
    public void removeTask(Task task) {
        if (!tasks.contains(task)) {
            throw new InvalidUserInputException(
                    String.format(TASK_DOES_NOT_EXIST,
                            task.getClass().getSimpleName().replace("Impl", ""),
                            task.getId(),
                            getName()));
        }

        historyOfChanges.addChange(
                String.format(BOARD_TASK_REMOVED,
                        task.getClass().getSimpleName().replace("Impl", ""),
                        task.getId(),
                        getName()));
        tasks.remove(task);
    }

    @Override
    public String toString() {
        return String.format("Board name: %s - Board items: %d",
                getName(), getTasks().size());
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, BOARD_NAME_LENGTH_ERROR);
        this.name = name;
    }
}
