package com.company.models.teams;

import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.history.ActivityLoggerImpl;
import com.company.models.history.Event;
import com.company.models.tasks.contracts.AssignableTask;
import com.company.models.teams.contracts.Member;
import com.company.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {

    public static final int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    private static final String NAME_LENGTH_ERROR = String.format(
            "A person cannot have less than %d symbols or more than %d symbols.",
            NAME_MIN_LENGTH, NAME_MAX_LENGTH);

    public static final String USER_TASK_ASSIGNED = "%s with ID %d assigned to user %s.";
    public static final String USER_TASK_UNASSIGNED = "%s with ID %d unassigned from user %s.";

    public static final String TASK_ALREADY_ASSIGNED_TO_USER = "%s with %d is already assigned to user %s";
    public static final String TASK_NOT_ASSIGNED_TO_USER = "%s with %d is not assigned to user %s";


    private final ActivityLoggerImpl historyOfChanges;
    private final List<AssignableTask> assignedTasks;
    private String name;

    public MemberImpl(String name) {
        this.historyOfChanges = new ActivityLoggerImpl();
        this.assignedTasks = new ArrayList<>();
        setName(name);
    }


    @Override
    public List<AssignableTask> getAssignedTasks() {
        return new ArrayList<>(assignedTasks);
    }

    @Override
    public String getName() { return name; }

    @Override
    public List<Event> getHistory() {
        return historyOfChanges.getCompleteHistory();
    }

    @Override
    public void recordActivity(String description) {
        historyOfChanges.addChange(description);
    }

    @Override
    public void assignTask(AssignableTask task) {
        if (assignedTasks.contains(task)) {
            throw new InvalidUserInputException(
                    String.format(TASK_ALREADY_ASSIGNED_TO_USER,
                            task.getClass().getSimpleName().replace("Impl", ""),
                            task.getId(),
                            getName()));
        }

        historyOfChanges.addChange(
                String.format(USER_TASK_ASSIGNED,
                        task.getClass().getSimpleName().replace("Impl", ""),
                        task.getId(),
                        getName()));

        task.setAssignee(getName());
        assignedTasks.add(task);
    }

    @Override
    public void unAssignTask(AssignableTask task) {
        if (!assignedTasks.contains(task)) {
            throw new InvalidUserInputException(
                    String.format(TASK_NOT_ASSIGNED_TO_USER,
                            task.getClass().getSimpleName().replace("Impl", ""),
                            task.getId(),
                            getName()));
        }

        historyOfChanges.addChange(
                String.format(USER_TASK_UNASSIGNED,
                        task.getClass().getSimpleName().replace("Impl", ""),
                        task.getId(),
                        getName()));

        task.unAssign();
        assignedTasks.remove(task);
    }

    @Override
    public String toString() {
        return String.format("User: %s - Tasks: %d%n",
                getName(),
                getAssignedTasks().size());
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, NAME_LENGTH_ERROR);
        this.name = name;
    }
}
