package com.company.models.teams.contracts;

public interface Nameable {
    String getName();
}
