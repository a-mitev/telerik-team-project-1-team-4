package com.company.models.teams.contracts;

import com.company.models.history.GetHistory;
import com.company.models.tasks.contracts.Task;

import java.util.List;

public interface Board extends GetHistory, Nameable {

    List<Task> getTasks();

    void addTask(Task task);

    void removeTask(Task task);
}
