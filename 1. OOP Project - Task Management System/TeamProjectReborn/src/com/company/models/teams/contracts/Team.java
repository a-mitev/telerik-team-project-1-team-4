package com.company.models.teams.contracts;

import com.company.models.history.GetHistory;
import com.company.models.tasks.contracts.Task;

import java.util.List;

public interface Team extends Nameable, GetHistory {

    List<Member> getMembers();

    List<Board> getBoards();

    void addMember(Member member);

    void removeMember(Member member);

    void addBoard(Board board);

    void removeBoard(Board board);

    boolean containsTask(Task task);


}
