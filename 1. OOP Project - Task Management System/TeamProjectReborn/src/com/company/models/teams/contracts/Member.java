package com.company.models.teams.contracts;

import com.company.models.history.GetHistory;
import com.company.models.tasks.contracts.AssignableTask;

import java.util.List;

public interface Member extends Nameable, GetHistory {

    List<AssignableTask> getAssignedTasks();

    void recordActivity(String description);

    void assignTask(AssignableTask task);

    void unAssignTask(AssignableTask task);

}
