package com.company.models.teams;

import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.history.ActivityLogger;
import com.company.models.history.ActivityLoggerImpl;
import com.company.models.history.Event;
import com.company.models.history.GetHistory;
import com.company.models.tasks.contracts.Task;
import com.company.models.teams.contracts.Board;
import com.company.models.teams.contracts.Member;
import com.company.models.teams.contracts.Team;
import com.company.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeamImpl implements Team {

    public static final int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    private static final String NAME_LENGTH_ERROR = String.format(
            "A team name cannot have less than %d or more than %d symbols.",
            NAME_MIN_LENGTH, NAME_MAX_LENGTH);

    String CREATION = "Team %s created.";
    String ADDITION = "User %s added.";
    String REMOVAL = "User %s removed.";
    String BOARD_ADDITION = "Board %s added.";
    String BOARD_REMOVAL = "Board %s removed.";

    public static final String MEMBER_NOT_IN_TEAM = "Member %s is not in team %s";
    public static final String MEMBER_ALREADY_IN_TEAM = "Member %s is already in team %s";

    public static final String BOARD_NOT_IN_TEAM = "Board %s is not in team %s";
    public static final String BOARD_ALREADY_IN_TEAM = "Board %s is already in team %s";



    private final List<Member> membersList;
    private final List<Board> boardsList;
    private final ActivityLogger historyOfChanges;
    private String name;

    public TeamImpl(String name) {
        setName(name);
        membersList = new ArrayList<>();
        boardsList = new ArrayList<>();
        historyOfChanges = new ActivityLoggerImpl();
        historyOfChanges.addChange(String.format(CREATION, getName()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(membersList);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boardsList);
    }

    @Override
    public void addMember(Member member) {
        if (getMembers().contains(member)) {
            throw new InvalidUserInputException(
                    String.format(MEMBER_ALREADY_IN_TEAM, member.getName(), getName()));
        }

        membersList.add(member);
        historyOfChanges.addChange(String.format(ADDITION, member.getName()));
    }

    @Override
    public void removeMember(Member member) {
        if (!getMembers().contains(member)) {
            throw new InvalidUserInputException(
                    String.format(MEMBER_NOT_IN_TEAM, member.getName(), getName()));
        }

        membersList.remove(member);
        historyOfChanges.addChange(String.format(REMOVAL, member.getName()));
    }

    @Override
    public void addBoard(Board board) {
        if (getBoards().contains(board)) {
            throw new InvalidUserInputException(
                    String.format(BOARD_ALREADY_IN_TEAM, board.getName(), getName()));
        }

        boardsList.add(board);
        historyOfChanges.addChange(String.format(BOARD_ADDITION, board.getName()));
    }

    @Override
    public void removeBoard(Board board) {
        if (!getBoards().contains(board)) {
            throw new InvalidUserInputException(
                    String.format(BOARD_NOT_IN_TEAM, board.getName(), getName()));
        }

        boardsList.remove(board);
        historyOfChanges.addChange(String.format(BOARD_REMOVAL, board.getName()));
    }

    @Override
    public boolean containsTask(Task task) {
        for (Board board : boardsList) {
            if (board.getTasks().contains(task)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Event> getHistory() {
        return historyOfChanges.getCompleteHistory();
    }

    @Override
    public String toString() {
        return String.format("Team: %s - Users: %d - Boards: %d - Tasks: %d | ",
                getName(),
                getMembers().size(),
                getBoards().size(),
                getBoards().stream().mapToInt(board -> board.getTasks().size()).sum());
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, NAME_LENGTH_ERROR);
        this.name = name;
    }
}
