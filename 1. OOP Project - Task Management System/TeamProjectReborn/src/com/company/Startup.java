package com.company;

import com.company.core.TaskManagementSystemEngineImpl;
import com.company.core.contracts.TaskManagementsSystemEngine;

public class Startup {

    public static void main(String[] args) {
        TaskManagementsSystemEngine engine = new TaskManagementSystemEngineImpl();
        engine.start();
    }
}
