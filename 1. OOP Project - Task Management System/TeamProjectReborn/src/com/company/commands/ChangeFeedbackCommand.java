package com.company.commands;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.FeedbackImpl;
import com.company.models.tasks.contracts.Feedback;
import com.company.models.tasks.enums.FeedbackStatus;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class ChangeFeedbackCommand extends BaseCommand{

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;
    public static final String INVALID_PROPERTY = "Invalid property to change has been provided.";
    public static final String RECORD_ACTIVITY = "Member %s changed the %s of %s with name %s to %s.";
    public static final String PROPERTY_UPDATED = "%s of %s with name %s has been changed to %s.";
    public static final String FEEDBACK_RATING_ERROR = "Feedback rating should be a numeric value between 0 and 10!";

    public ChangeFeedbackCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);
        Member user = getRepository().findMemberByName(parameters.get(0).trim());
        String feedbackName = parameters.get(1);
        String propertyToChange = parameters.get(2).trim();
        String newValue = parameters.get(3).toUpperCase();

        Feedback feedback = getRepository().findFeedbackByTitle(feedbackName);

        switch (propertyToChange.toUpperCase()) {
            case "RATING":
                int rating = ParsingHelpers.tryParseInt(newValue, FEEDBACK_RATING_ERROR);
                ValidationHelpers.validateIntRange(
                        rating, FeedbackImpl.RATING_MIN, FeedbackImpl.RATING_MAX, FEEDBACK_RATING_ERROR);
                feedback.setRating(rating);
                break;
            case "STATUS":
                FeedbackStatus status = ParsingHelpers.tryParseEnum(newValue, FeedbackStatus.class);
                feedback.setStatus(status);
                break;
            default:
                throw new InvalidUserInputException(INVALID_PROPERTY);
        }

        user.recordActivity(
                String.format(RECORD_ACTIVITY, user.getName(), propertyToChange, "Feedback", feedbackName, newValue));

        return String.format(PROPERTY_UPDATED, propertyToChange, "Feedback", feedbackName, newValue);
    }
}
