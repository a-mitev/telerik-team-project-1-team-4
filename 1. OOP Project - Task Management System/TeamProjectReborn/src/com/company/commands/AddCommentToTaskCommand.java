package com.company.commands;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.comments.Comment;
import com.company.models.comments.CommentImpl;
import com.company.models.tasks.contracts.Task;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class AddCommentToTaskCommand extends BaseCommand{

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public static final String INVALID_ID = "Invalid task Id.";
    public static final String COMMENT_EVENT = "User %s added a comment to %s with ID %d.";

    public AddCommentToTaskCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Member author = getRepository().findMemberByName(parameters.get(0));
        int Id = ParsingHelpers.tryParseInt(parameters.get(1), INVALID_ID);
        Task task = getRepository().findTaskById(Id);
        String content = parameters.get(2);
        getRepository().validateUserAndTaskFromSameTeam(author.getName(), task.getId());

        Comment comment = new CommentImpl(content, author.getName());

        task.addComment(comment);

        String taskType = task.getClass().getSimpleName().replace("Impl", "");
        author.recordActivity(String.format(COMMENT_EVENT, author.getName(), taskType, task.getId()));
        return String.format(COMMENT_EVENT, author.getName(), taskType, task.getId());
    }
}
