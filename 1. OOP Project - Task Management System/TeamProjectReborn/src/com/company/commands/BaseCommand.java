package com.company.commands;

import com.company.commands.contracts.Command;
import com.company.core.contracts.TaskManagementRepository;

import java.util.List;

public abstract class BaseCommand implements Command {

    public static final String USER_CREATED_TASK = "User %s created a new %s in board %s.";

    private final TaskManagementRepository repository;

    protected BaseCommand(TaskManagementRepository repository) {
        this.repository = repository;
    }

    protected TaskManagementRepository getRepository(){
        return repository;
    }

    @Override
    public String execute(List<String> parameters) {
        return executeCommand(parameters);
    }

    protected abstract String executeCommand(List<String> parameters);
}
