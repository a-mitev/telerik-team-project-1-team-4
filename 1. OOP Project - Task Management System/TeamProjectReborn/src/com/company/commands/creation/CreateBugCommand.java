package com.company.commands.creation;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.tasks.enums.BugSeverity;
import com.company.models.tasks.enums.Priority;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CreateBugCommand extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 9;


    public CreateBugCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);
        Member member = getRepository().findMemberByName(parameters.get(0));
        String teamName = parameters.get(1);
        getRepository().checkIfMemberIsFromTeam(member.getName(), teamName);
        String boardName = parameters.get(2);
        String title = parameters.get(3);
        String description = parameters.get(4);
        List<String> stepsToReproduce = Arrays.stream(parameters.get(5).split("[!?;\\.]"))
                .collect(Collectors.toList());
        Priority priority = ParsingHelpers.tryParseEnum(parameters.get(6), Priority.class);
        BugSeverity severity = ParsingHelpers.tryParseEnum(parameters.get(7), BugSeverity.class);
        String assignee = parameters.get(8);
        //Check if
        member.recordActivity(String.format(USER_CREATED_TASK, member.getName(), "Bug", boardName));

        return getRepository().createNewBug(teamName, boardName, title,
                description, stepsToReproduce, priority, severity, assignee);
    }
}
