package com.company.commands.creation;

import com.company.commands.BaseCommand;
import com.company.commands.contracts.Command;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class CreateTeamCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String ALREADY_EXISTS = "This %s name already exists! Please choose a unique %s name.";

    public CreateTeamCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = parameters.get(0);

        if (getRepository().getTeams().stream().anyMatch(team -> team.getName().equals(teamName))) {
            throw new InvalidUserInputException(String.format(ALREADY_EXISTS, "team", "team"));
        }

        return getRepository().createNewTeam(teamName);
    }
}
