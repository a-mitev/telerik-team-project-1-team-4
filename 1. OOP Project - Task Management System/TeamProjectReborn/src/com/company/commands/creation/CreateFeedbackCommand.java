package com.company.commands.creation;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.tasks.FeedbackImpl;
import com.company.models.tasks.enums.FeedbackStatus;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class CreateFeedbackCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 6;
    public static final String FEEDBACK_RATING_ERROR = "Feedback rating should be a numeric value between 0 and 10!";

    public CreateFeedbackCommand(TaskManagementRepository repository) {super(repository);}

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);

        Member member = getRepository().findMemberByName(parameters.get(0));
        String teamName = parameters.get(1);
        //Check if
        getRepository().checkIfMemberIsFromTeam(member.getName(), teamName);
        String boardName = parameters.get(2);
        String title = parameters.get(3);
        String description = parameters.get(4);
        int rating = ParsingHelpers.tryParseInt(parameters.get(5), FEEDBACK_RATING_ERROR);
        ValidationHelpers.validateIntRange(rating, FeedbackImpl.RATING_MIN,
                FeedbackImpl.RATING_MAX, FEEDBACK_RATING_ERROR);

        member.recordActivity(String.format(USER_CREATED_TASK, member.getName(), "Feedback", boardName));

        return getRepository().createNewFeedback(teamName, boardName, title, description, rating);
    }
}
