package com.company.commands.creation;

import com.company.commands.BaseCommand;
import com.company.commands.contracts.Command;
import com.company.core.contracts.TaskManagementRepository;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class CreateNewMember extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public CreateNewMember(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String memberName = parameters.get(0);
        return getRepository().createNewMember(memberName);
    }
}
