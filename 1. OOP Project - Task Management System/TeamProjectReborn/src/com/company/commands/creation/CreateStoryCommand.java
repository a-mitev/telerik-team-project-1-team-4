package com.company.commands.creation;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.tasks.enums.Priority;
import com.company.models.tasks.enums.StorySize;
import com.company.models.tasks.enums.StoryStatus;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class CreateStoryCommand extends BaseCommand {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 8;

    public CreateStoryCommand(TaskManagementRepository repository) {
        super(repository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Member member = getRepository().findMemberByName(parameters.get(0));
        String teamName = parameters.get(1);
        String boardName = parameters.get(2);
        String title = parameters.get(3);
        String description = parameters.get(4);
        Priority priority = ParsingHelpers.tryParseEnum(parameters.get(5), Priority.class);
        StorySize size = ParsingHelpers.tryParseEnum(parameters.get(6), StorySize.class);
        String assignee = parameters.get(7);
        //Checks if
        getRepository().checkIfMemberIsFromTeam(member.getName(), teamName);
        //Check if

        member.recordActivity(String.format(USER_CREATED_TASK, member.getName(), "Story", boardName));

        return getRepository().createNewStory(teamName, boardName, title, description, priority, size, assignee);
    }
}

