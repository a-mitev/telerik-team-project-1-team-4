package com.company.commands;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.Story;
import com.company.models.tasks.enums.Priority;
import com.company.models.tasks.enums.StorySize;
import com.company.models.tasks.enums.StoryStatus;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class ChangeStoryCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;
    public static final String INVALID_PROPERTY = "Invalid property to change has been provided.";
    public static final String RECORD_ACTIVITY = "Member %s changed the %s of %s with name %s to %s.";
    public static final String PROPERTY_UPDATED = "%s of %s with name %s has been changed to %s.";

    public ChangeStoryCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Member member = getRepository().findMemberByName(parameters.get(0).trim());
        String storyName = parameters.get(1);
        String propertyToChange = parameters.get(2).trim();
        String newValue = parameters.get(3).toUpperCase();

        Story story = getRepository().findStoryByTitle(storyName);

        switch (propertyToChange.toUpperCase()) {
            case "PRIORITY":
                Priority priority = ParsingHelpers.tryParseEnum(newValue, Priority.class);
                story.setPriority(priority);
                break;
            case "SIZE":
                StorySize size = ParsingHelpers.tryParseEnum(newValue, StorySize.class);
                story.setSize(size);
                break;
            case "STATUS":
                StoryStatus status = ParsingHelpers.tryParseEnum(newValue, StoryStatus.class);
                story.setStatus(status);
                break;
            default:
                throw new InvalidUserInputException(INVALID_PROPERTY);
        }

        member.recordActivity(
                String.format(RECORD_ACTIVITY, member.getName(), propertyToChange, "Story", storyName, newValue));

        return String.format(PROPERTY_UPDATED, propertyToChange, "Story", storyName, newValue);
    }
}
