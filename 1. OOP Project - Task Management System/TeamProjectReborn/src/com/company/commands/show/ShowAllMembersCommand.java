package com.company.commands.show;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.teams.contracts.Member;

import java.util.List;

public class ShowAllMembersCommand extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    public ShowAllMembersCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        List<Member> listOfAllMembers = getRepository().getMembers();

        if (listOfAllMembers.isEmpty()) return "There are no registered members.";

        return showUsers(listOfAllMembers);
    }
    private String showUsers(List<Member> members) {
        StringBuilder output = new StringBuilder();

        output.append("--MEMBERS--");
        for (int i = 0; i < members.size(); i++) {
            output.append(String.format("%n%d. %s", i+1, members.get(i).toString()));
        }

        return output.toString();
    }

}
