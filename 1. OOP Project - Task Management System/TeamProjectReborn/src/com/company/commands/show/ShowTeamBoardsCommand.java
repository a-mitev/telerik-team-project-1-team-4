package com.company.commands.show;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.teams.contracts.Board;
import com.company.models.teams.contracts.Team;
import com.company.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class ShowTeamBoardsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String NO_ITEMS_TO_DISPLAY = "No %s to display.";
    public ShowTeamBoardsCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Team team = getRepository().findTeamByName(parameters.get(0));

        if (team.getBoards().isEmpty()) {
            return String.format(NO_ITEMS_TO_DISPLAY, "boards");
        }

        return team.getBoards()
                .stream()
                .map(Board::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
