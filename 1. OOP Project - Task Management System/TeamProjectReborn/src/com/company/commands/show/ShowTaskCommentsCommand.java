package com.company.commands.show;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.comments.Comment;
import com.company.models.tasks.contracts.Task;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class ShowTaskCommentsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public static final String NO_ITEMS_TO_DISPLAY = "No %s to display.";

    public static final String INVALID_ID = "Invalid task Id.";

    public ShowTaskCommentsCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        int Id = ParsingHelpers.tryParseInt(parameters.get(0), INVALID_ID);

        Task task = getRepository().findTaskById(Id);

        if (task.getComments().isEmpty()) {
            return String.format(NO_ITEMS_TO_DISPLAY, "comments");
        }

        return task.
                getComments()
                .stream()
                .map(Comment::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
