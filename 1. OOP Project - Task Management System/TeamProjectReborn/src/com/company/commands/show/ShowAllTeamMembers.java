package com.company.commands.show;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.teams.contracts.Member;
import com.company.models.teams.contracts.Team;
import com.company.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class ShowAllTeamMembers extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String NO_ITEMS_TO_DISPLAY = "No %s to display.";

    public ShowAllTeamMembers(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Team team = getRepository().findTeamByName(parameters.get(0));

        if (team.getMembers().isEmpty()) {
            return String.format(NO_ITEMS_TO_DISPLAY, "users");
        }

        return team.getMembers()
                .stream()
                .map(Member::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
