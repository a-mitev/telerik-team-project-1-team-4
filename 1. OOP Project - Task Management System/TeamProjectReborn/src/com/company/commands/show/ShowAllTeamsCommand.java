package com.company.commands.show;

import com.company.commands.BaseCommand;
import com.company.core.contracts.TaskManagementRepository;
import com.company.models.teams.contracts.Team;
import com.company.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class ShowAllTeamsCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;
    public static final String NO_ITEMS_TO_DISPLAY = "No %s to display.";

    public ShowAllTeamsCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        List<Team> teams = getRepository().getTeams();
        if (teams.isEmpty()) {
            return String.format(NO_ITEMS_TO_DISPLAY, "teams");
        }
        StringBuilder output = new StringBuilder();
        teams.forEach(output::append);
        return output.toString().trim();
    }
}
