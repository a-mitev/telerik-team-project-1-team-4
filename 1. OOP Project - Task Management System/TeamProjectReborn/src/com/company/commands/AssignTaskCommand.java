package com.company.commands;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.AssignableTask;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class AssignTaskCommand extends BaseCommand{

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public static final String INVALID_ID = "Invalid task ID.";
    public static final String UNASSIGNED = "Unassigned";
    public static final String ASSIGN_EVENT = "User %s assigned %s with ID %d to %s.";
    public static final String CANNOT_REASSIGN_TO_SAME_USER = "Task is already assigned to %s.";

    public AssignTaskCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        Member assigner = getRepository().findMemberByName(parameters.get(0));
        int Id = ParsingHelpers.tryParseInt(parameters.get(1), INVALID_ID);

        AssignableTask task = getRepository().findAssignableTask(Id);
        Member newAssignee = getRepository().findMemberByName(parameters.get(2));
        //Check if
        getRepository().validateUserAndTaskFromSameTeam(assigner.getName(), task.getId());
        //Check if
        getRepository().validateUserAndTaskFromSameTeam(newAssignee.getName(), task.getId());

        if (assigner.getName().equals(newAssignee.getName())) {
            throw new InvalidUserInputException(String.format(CANNOT_REASSIGN_TO_SAME_USER, assigner));
        }

        if (!task.getAssignee().equals(UNASSIGNED)) {
            Member oldAssignee = getRepository().findMemberByName(task.getAssignee());
            oldAssignee.unAssignTask(task);
        }

        newAssignee.assignTask(task);

        String taskType = task.getClass().getSimpleName().replace("Impl", "");
        assigner.recordActivity(String.format(ASSIGN_EVENT, assigner.getName(),
                taskType, task.getId(), newAssignee.getName()));
        return String.format(ASSIGN_EVENT, assigner.getName(), taskType, task.getId(), newAssignee.getName());
    }
}
