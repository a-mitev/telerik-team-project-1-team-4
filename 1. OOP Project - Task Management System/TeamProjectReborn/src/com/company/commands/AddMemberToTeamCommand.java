package com.company.commands;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.teams.contracts.Member;
import com.company.models.teams.contracts.Team;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class AddMemberToTeamCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String USER_ADDED_TO_TEAM = "User %s successfully added to team %s.";
    public static final String USER_ALREADY_ON_TEAM = "Member %s is already part of team %s!";

    public AddMemberToTeamCommand(TaskManagementRepository repository) { super(repository); }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        //Check if
        Member member = getRepository().findMemberByName(parameters.get(0));
        Team team = getRepository().findTeamByName(parameters.get(1));
        //Check if
        if (team.getMembers().contains(member)) {
            throw new IllegalArgumentException(
                    String.format(USER_ALREADY_ON_TEAM, member.getName(), team.getName()));
        }

        team.addMember(member);
        return String.format(USER_ADDED_TO_TEAM, member.getName(), team.getName());
    }
}

