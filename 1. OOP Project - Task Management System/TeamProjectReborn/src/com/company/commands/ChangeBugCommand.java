package com.company.commands;

import com.company.core.contracts.TaskManagementRepository;
import com.company.models.exceptions.InvalidUserInputException;
import com.company.models.tasks.contracts.Bug;
import com.company.models.tasks.enums.BugSeverity;
import com.company.models.tasks.enums.BugStatus;
import com.company.models.tasks.enums.Priority;
import com.company.models.teams.contracts.Member;
import com.company.utils.ParsingHelpers;
import com.company.utils.ValidationHelpers;

import java.util.List;

public class ChangeBugCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;
    public static final String INVALID_PROPERTY = "Invalid property to change has been provided.";
    public static final String RECORD_ACTIVITY = "User %s changed the %s of %s with name %s to %s.";
    public static final String PROPERTY_UPDATED = "%s of %s with name %s has been changed to %s.";

    public ChangeBugCommand(TaskManagementRepository repository) { super(repository); }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        Member member = getRepository().findMemberByName(parameters.get(0).trim());
        String bugName = parameters.get(1);
        String propertyToChange = parameters.get(2).trim();
        String newValue = parameters.get(3).toUpperCase();

        Bug bug = getRepository().findBugByTitle(bugName);

        switch (propertyToChange.toUpperCase()) {
            case "PRIORITY":
                Priority priority = ParsingHelpers.tryParseEnum(newValue, Priority.class);
                bug.setPriority(priority);
                break;
            case "SEVERITY":
                BugSeverity severity = ParsingHelpers.tryParseEnum(newValue, BugSeverity.class);
                bug.setSeverity(severity);
                break;
            case "STATUS":
                BugStatus status = ParsingHelpers.tryParseEnum(newValue, BugStatus.class);
                bug.setStatus(status);
                break;
            default:
                throw new InvalidUserInputException(INVALID_PROPERTY);
        }

        member.recordActivity(
                String.format(RECORD_ACTIVITY, member.getName(), propertyToChange, "Bug", bugName, newValue));

        return String.format(PROPERTY_UPDATED, propertyToChange, "Bug", bugName, newValue);

    }
}
